import React from 'react'
import Properties from './Properties'

const Dashboard = () => {
  return (
    <div className='container mt-5'>
        <h2>Propiedades</h2>
        <div className='row mt-5'>
        <table className="table table-hover">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Direccion</th>
      <th scope="col">Precio</th>
      <th scope="col">Detalles</th>
    </tr>
  </thead>
    <Properties></Properties>
</table>
        </div>
    </div>
  )
}

export default Dashboard