import React from 'react'
import { useParams } from 'react-router-dom/cjs/react-router-dom.min'
import Imagenes from './Imagenes'

export const Details = () => {

    const {id} = useParams()
    

    const [casa, setCasa] = React.useState([])
    
    const [urlNext, setUrlNext] = React.useState('https://apiproperties.azurewebsites.net/api/Properties/GetPropertieDetails?idProperty=' + id)
    
  

  React.useEffect(() => {
    obtenerDatos(urlNext)
    
  }, [])

  const obtenerDatos = async () => {
    
    const data = await fetch(urlNext)
    const propiedad = await data.json()
    
    setCasa(propiedad)
    
  }

  

  return (
    <div className='container mt-5 h-50'>
        <h3 className='text-center'>Informacion de Vivienda</h3>
        <div className='row h-100 mt-5'>
            <div className='col-lg'>
                <label className='form-control text-uppercase'>Nombre Vivienda:  <strong>{casa.name}</strong></label>
                <label className='form-control text-uppercase'>Direccion Vivienda:  <strong>{casa.address}</strong></label>
                <label className='form-control text-uppercase'>Precio de Venta:  <strong>{casa.price}</strong></label>
                <label className='form-control text-uppercase'>Año de construccion:  <strong>{casa.year}</strong></label>
            </div>
        </div>
        <hr/>
        <Imagenes idpropiedad={id}></Imagenes>
    </div>
  )
}
