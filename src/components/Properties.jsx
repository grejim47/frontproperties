import React from 'react'
import { Link } from 'react-router-dom'


const Properties = () => {

const [casas, setCasas] = React.useState([])
const [urlNext, setUrlNext] = React.useState('https://apiproperties.azurewebsites.net/api/Properties/GetProperties')
  

  React.useEffect(() => {
    obtenerDatos(urlNext)
  }, [])

  const obtenerDatos = async () => {
    
    const data = await fetch(urlNext)
    const propiedades = await data.json()
    console.log(propiedades)
    setCasas(propiedades)
    
  }
  return (
      <tbody>
        {
          casas.map((item,index) => (
            <tr key={index} scope="row">
            <th >{item.idProperty}</th>
            <td>{item.name}</td>
            <td>{item.address}</td>
            <td>{item.price}</td>
            <td>
                <Link to={`/details/${item.idProperty}`}>
                    Ver detalles
                </Link>
            </td>
          </tr>
          ))
        }
    </tbody>
  )
}

export default Properties