import React from 'react'
import { useParams } from 'react-router-dom/cjs/react-router-dom.min'
import Carousel from 'react-img-carousel';
require('react-img-carousel/lib/carousel.css');

const Imagenes = () => {

    const {id} = useParams()
   
    const [urlImg, setUrlImg] = React.useState('https://apiproperties.azurewebsites.net/api/Properties/GetPropertieImages?idProperty=' + id)
    const [imagenes, setImagenes] = React.useState([])
    

    React.useEffect(() => {        
        obtenerImagenes()
      }, [])
      
      
    const obtenerImagenes = async () => {
    
        const data = await fetch(urlImg)
        const img = await data.json()
        const list = []
        img.map((item,index) => (
            list.push({image: item.files})
          ))
          setImagenes(
            list
        )
          console.log(imagenes)
      }
  return (
    <div className='text-center align-items-center'>
        <h6 className='text-muted'>Se deben agregar urls de imagenes a las propiedades para que se muestren</h6>
                        
        <Carousel viewportWidth="100%"  cellPadding={ 5 }>
        <img src='http://www.grandessoluciones.com/wp-content/uploads/se-vende-casa-market-inmobiliaria.jpg' alt='img'/>
            {imagenes.map((item,index) => (<img key={index} src={item.image}/>))}
        </Carousel>

    </div>
  )
}

export default Imagenes