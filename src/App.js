import logo from './logo.svg';
import './App.css';
import Dashboard from './components/Dashboard';
import { BrowserRouter as Router, Switch, Route, Link, NavLink } from 'react-router-dom'; 
import { Details } from './components/Details';
import 'react-slideshow-image/dist/styles.css'

function App() {
  return (
    <Router>
      <div className='container mt-5'>
        <div className='btn-group w-100'>
          <Link to="/" className='btn btn-dark'>Home</Link>
        </div>
      </div>
      <Switch>
      <Route path="/details/:id" component={Details}>
      </Route>
      <Route path="/" component={Dashboard}>
      </Route>
      </Switch>
    </Router>
  );
}

export default App;
